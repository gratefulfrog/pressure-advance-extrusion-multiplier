/*[Hidden]*/
$fn=100;

/*[Parameters]*/

FirstLayer  = 0.25;
OtherLayers = 0.1;
NbLayers    = 1; //1
Width       = 250;
Length      = 250;


module plate(){
  z = NbLayers== 1 ? FirstLayer : FirstLayer + (NbLayers-1) *OtherLayers;
  x = Width;
  y = Length;
  cube([x,y,z]);
  echo([x,y,z]);
}
plate();


